 ## How many % of banks & financial institutions in Oceania are using Cloudflare?

The following is a list of banks & financial institutions in Oceania.


<details>
<summary>_click me_

## Australia
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| AMP Limited | https://www.amp.com.au/ | No |
| Australia and New Zealand Banking Group | https://www.anz.com/ | Yes |
| Auswide Bank | https://www.auswidebank.com.au/ | No |
| Bank Australia | https://www.bankaust.com.au/ | Yes |
| Bank First | https://www.bankfirst.com.au/ | Yes |
| Bank of Melbourne | https://www.bankofmelbourne.com.au/ | Yes |
| Bank of Queensland | https://www.boq.com.au/ | Yes |
| BankSA | https://www.banksa.com.au/ | No |
| BankVic | https://www.bankvic.com.au/ | No |
| Bankwest | https://www.bankwest.com.au/ | No |
| Bendigo and Adelaide Bank | https://www.bendigoadelaide.com.au/ | Yes |
| Beyond Bank Australia | https://beyondbank.com.au/ | Yes |
| Clean Energy Finance Corporation | https://www.cefc.com.au/ | No |
| Commonwealth Bank | https://commbank.com.au/ | No |
| Greater Bank | https://www.greater.com.au/ | Yes |
| Heritage Bank | https://heritage.com.au/ | No |
| Hume Bank | https://www.humebank.com.au/ | Yes |
| IMB Bank | https://www.imb.com.au/ | No |
| Macquarie Group | https://www.macquarie.com/ | No |
| ME Bank | https://www.mebank.com.au/ | No |
| MyState Limited | https://www.mystate.com.au/ | No |
| National Australia Bank | https://nab.com.au/ | No |
| Newcastle Permanent Building Society | https://www.newcastlepermanent.com.au/ | No |
| P&N Bank | https://pnbank.com.au/ | No |
| Police Bank | https://www.policebank.com.au/ | No |
| Qudos Bank | https://www.qudosbank.com.au/ | No |
| Regional Australia Bank | https://www.regionalaustraliabank.com.au/ | Yes |
| St.George Bank | https://www.stgeorge.com.au/ | No |
| Suncorp Bank | https://www.suncorp.com.au/ | Yes |
| Teachers Mutual Bank | https://www.tmbank.com.au/ | Yes |
| Tyro Payments | https://tyro.com/ | Yes |
| UBank | https://www.ubank.com.au/ | Yes |
| Unity Bank of Canada | https://www.unitybank.com/ | Yes |
| Up Money Pty Ltd | https://up.com.au/ | No |
| Volt Bank | https://www.voltbank.com.au/ | Yes |
| Westpac | https://westpac.com.au/ | No |
| Arab Bank | https://www.arabbank.com/ | No |
| Bank of China | https://www.boc.cn/ | No |
| Citibank Australia | https://www.citibank.com.au/ | No |
| HSBC Bank Australia | https://www.hsbc.com.au/ | No |
| HDFC Bank | https://www.hdfcbank.com/ | Yes |
| ING Australia | https://www.ing.com.au/ | No |
| Rabobank | https://www.rabobank.com/ | No |
| ABN AMRO | https://www.abnamro.com/ | No |
| Bank of America | https://www.bankofamerica.com/ | Yes |
| Bank of Communications | https://www.bankcomm.com/ | No |
| Barclays Investment Bank | https://www.investmentbank.barclays.com/ | No |
| BNP Paribas | https://group.bnpparibas/ | No |
| China Everbright Bank | https://www.cebbank.com/ | No |
| Credit Suisse | https://www.credit-suisse.com/ | No |
| Deutsche Bank | https://db.com/ | Yes |
| HBOS | https://www.hbosplc.com/ | No |
| HSBC Bank | https://hsbc.co.uk/ | No |
| ING Group | https://ing.com/ | No |
| JPMorgan Chase | https://www.jpmorganchase.com/ | Yes |
| Mizuho Corporate Bank | https://www.mizuhocbk.com/ | No |
| National Bank of Greece | https://www.nbg.gr/ | Yes |
| OCBC Bank | https://www.ocbc.com/ | No |
| Royal Bank of Canada | https://rbc.com/ | No |
| Société Générale | https://societegenerale.com/ | Yes |
| Standard Chartered | https://www.sc.com/ | No |
| Sumitomo Mitsui Banking Corporation | https://www.smbcgroup.com/ | No |
| MUFG Bank | https://www.bk.mufg.jp/ | No |
| The Hongkong and Shanghai Banking Corporation | https://www.hsbc.com.hk/ | No |
| Northern Trust | https://www.northerntrust.com/ | Yes |
| Royal Bank of Scotland | https://www.rbs.co.uk/ | Yes |
| UBS | https://www.ubs.com/ | No |
| United Overseas Bank | https://www.uobgroup.com/ | No |
| Woori Bank | https://eng.wooribank.com/ | No |

</details>


<details>
<summary>_click me_

## Fiji
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Bank of Baroda | https://www.bankofbaroda.com/ | No |
| Reserve Bank of Fiji | https://www.rbf.gov.fj/ | Yes |
| Bank South Pacific | https://www.bsp.com.pg/ | Yes |
| ANZ Fiji | https://www.anz.com/ | Yes |
| Westpac | https://westpac.co.nz/ | No |

</details>


<details>
<summary>_click me_

## Kingdom of Tonga
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| National Reserve Bank of Tonga | https://www.reservebank.to/ | No |
| Australia and New Zealand Banking Group | https://www.anz.com/ | Yes |
| Bank South Pacific | https://www.bsp.com.pg/ | Yes |
| Tonga Development Bank | https://www.tdb.to/ | No |
| Pacific International Commercial Bank | https://www.pacific-international.com/ | No |

</details>


<details>
<summary>_click me_

## New Zealand
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Reserve Bank of New Zealand | https://www.rbnz.govt.nz/ | Yes |
| Bank of New Zealand | https://www.bnz.co.nz/ | No |
| ANZ Bank New Zealand | https://www.anz.co.nz/ | No |
| ASB Bank | https://www.asb.co.nz/ | No |
| Westpac | https://westpac.co.nz/ | No |
| Heartland Bank | https://www.heartland.co.nz/ | Yes |
| Kiwibank | https://www.kiwibank.co.nz/ | No |
| SBS Bank | https://www.sbsbank.co.nz/ | No |
| The Co-operative Bank | https://www.co-operativebank.co.nz/ | No |
| TSB | https://www.tsb.co.nz/ | No |
| Rabobank New Zealand | https://www.rabobank.co.nz/ | No |
| HSBC | https://www.hsbc.com/ | Yes |
| Bank of India | https://www.bankofindia.co.in/ | No |
| Citibank | https://citi.com/ | No |
| China Construction Bank | https://www.ccb.com/ | No |

</details>


<details>
<summary>_click me_

## Papua New Guinea
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Bank of Papua New Guinea | https://www.bankpng.gov.pg/ | Yes |
| Bank South Pacific | https://www.bsp.com.pg/ | Yes |

</details>


<details>
<summary>_click me_

## Solomon Islands
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| National Bank of Solomon Islands | https://www.bsp.com.sb/ | No |
| Bank South Pacific | https://www.bsp.com.pg/ | Yes |

</details>


<details>
<summary>_click me_

## Vanuatu
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Reserve Bank of Vanuatu | https://www.rbv.gov.vu/ | No |
| Australia and New Zealand Banking Group | https://www.anz.com/ | Yes |
| Bank South Pacific | https://www.bsp.com.pg/ | Yes |
| National Bank of Vanuatu | https://www.nbv.vu/ | No |

</details>


<details>
<summary>_click me_

## Western Samoa
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Samoa | https://www.cbs.gov.ws/ | Yes |
| Australia and New Zealand Banking Group | https://www.anz.com/ | Yes |
| Bank South Pacific | https://www.bsp.com.pg/ | Yes |
| National Bank of Samoa | https://www.nbs.ws/ | No |

</details>

---

### By Country

| Name | Cloudflared | Normal | CF % |
| --- | --- | --- | --- |
| Australia | 24 | 45 | 34.78% |
| Fiji | 3 | 2 | 60% |
| Kingdom of Tonga | 2 | 3 | 40% |
| New Zealand | 3 | 12 | 20% |
| Papua New Guinea | 2 | 0 | 100% |
| Solomon Islands | 1 | 1 | 50% |
| Vanuatu | 2 | 2 | 50% |
| Western Samoa | 3 | 1 | 75% |


### Total

| Type | Site Count | Owner Count |
| --- | --- | --- |
| Cloudflare | 40 | 31 |
| Normal | 66 | 65 |
| Total | 106 | 96 |


### 37.74% of banks & financial institutions sites are using Cloudflare.
### 32.29% of banks & financial institutions in Oceania are using Cloudflare.

