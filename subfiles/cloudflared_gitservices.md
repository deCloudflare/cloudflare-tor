## How many % of public Git services are using Cloudflare?


- [Gitea](https://en.wikipedia.org/wiki/Gitea)
```
Gitea is an open-source forge software package for hosting 
software development version control using Git as well as 
other collaborative features like bug tracking, wikis and code review
```

- [Gitlab](https://en.wikipedia.org/wiki/Gitlab)
```
GitLab is a web-based DevOps lifecycle tool that provides a Git-repository 
manager providing wiki, issue-tracking and continuous integration 
and deployment pipeline features, using an open-source license, developed by GitLab Inc.
```


Here's a list of public Git services.


<details>
<summary>_click me_

## Gitea, Gitlab, etc
</summary>

| Site | Cloudflared |
| --- | --- |
| allthe.codes | No |
| auth.doeber.nl | No |
| authoritah.io | No |
| bitbucket.org | No |
| black.uber.space | No |
| ccr.calibrate.be | No |
| ceregatti.com | No |
| code.antopie.org | No |
| code.briarproject.org | No |
| code.crosse.org | Yes |
| code.habd.as | No |
| code.ita-prog.pl | No |
| code.jnf.me | Yes |
| code.lag.net | No |
| code.netlandish.com | No |
| codeberg.org | No |
| codelens.dev | Yes |
| de.edumat.io | No |
| dev.lovelyhq.com | No |
| dev.sum7.eu | No |
| devheroes.codes | Yes |
| doc-user.qware.tech | No |
| drone.tildegit.org | No |
| erj4.uk | No |
| finallycoffee.eu | No |
| forge.chapril.org | No |
| framagit.org | No |
| g.plsnotracking.com | No |
| gameinfuser.com | No |
| git.53hor.net | No |
| git.acloud.one | Yes |
| git.activitypub.dev | No |
| git.aite.xyz | No |
| git.alles.cx | No |
| git.artezio.net | No |
| git.aslcontrol.com | No |
| git.augendre.info | No |
| git.b-ehlers.de | No |
| git.barbel.synology.me | No |
| git.bitlair.nl | No |
| git.bn4t.me | Yes |
| git.bsmg.dev | Yes |
| git.charlesreid1.com | No |
| git.chasekidder.com | Yes |
| git.chenguanzhou.com | No |
| git.control-design.pl | No |
| git.cslabs.clarkson.edu | No |
| git.cyberjinh.fr | No |
| git.data.coop | No |
| git.defectink.com | No |
| git.deuchnord.fr | No |
| git.devuan.org | No |
| git.disroot.org | No |
| git.dmxcontrol-projects.org | No |
| git.ecoservice24.de | Yes |
| git.educate.center | No |
| git.eeqj.de | No |
| git.elkood.com | Yes |
| git.etud.insa-toulouse.fr | No |
| git.fcloud.ovh | No |
| git.fdn.fr | No |
| git.feneas.org | No |
| git.finallycoffee.eu | No |
| git.finmechanics.com | No |
| git.freezer.life | Yes |
| git.fsfe.org | No |
| git.gaiaservice.fr | No |
| git.geekservice.de | No |
| git.govtop.cn | No |
| git.guildofwriters.org | No |
| git.hardenedbsd.org | Yes |
| git.hiitsdevin.dev | Yes |
| git.hnmediatech.com | No |
| git.honeypot.im | No |
| git.hopol.cn | No |
| git.hush.is | Yes |
| git.hya.sk | No |
| git.iamthefij.com | Yes |
| git.imrc.kist.re.kr | No |
| git.internal.services.oscarchou.com | No |
| git.it-neuhauser.de | No |
| git.ita-ausbildung.de | No |
| git.ixarea.com | No |
| git.jami.net | No |
| git.jeddunk.xyz | No |
| git.joinplu.me | No |
| git.kageru.moe | No |
| git.kaki87.net | No |
| git.kalli.st | No |
| git.kiwifarms.net | Yes |
| git.kknos.uber.space | No |
| git.klingt.net | No |
| git.koesters.xyz | No |
| git.kwarde.com | No |
| git.laquadrature.net | No |
| git.larlet.fr | No |
| git.lighttpd.net | No |
| git.madfire.net | No |
| git.madi-wka.club | Yes |
| git.mafiasi.de | No |
| git.marvid.fr | No |
| git.mastodont.cat | No |
| git.mentality.rip | No |
| git.mganczarczyk.pl | Yes |
| git.mind-tracker.com | No |
| git.minicloud.xyz | Yes |
| git.mitchellhansen.info | No |
| git.mutuellegsmc.fr | No |
| git.myceliandre.fr | No |
| git.nixnet.services | No |
| git.nogafam.es | No |
| git.obicloud.net | Yes |
| git.openprivacy.ca | No |
| git.oroques.dev | No |
| git.ovs.aktivbank-factoring.de | No |
| git.paintingofapples.com | No |
| git.passageenseine.fr | No |
| git.plsnotracking.com | No |
| git.plspnkt.uber.space | No |
| git.pofilo.fr | No |
| git.polytech-services-nancy.fr | No |
| git.pyrocko.org | No |
| git.radio.clubs.etsit.upm.es | No |
| git.rbcommunity.info | Yes |
| git.reclaimfutures.org | No |
| git.regar42.fr | No |
| git.riper.fr | No |
| git.rootevolution.clients.goautomate.ai | No |
| git.rys.io | No |
| git.safemobile.org | No |
| git.sagidayan.com | Yes |
| git.sangraha.xyz | No |
| git.sdf.org | No |
| git.service.liminalytics.com | No |
| git.service.wobcom.consulting | No |
| git.services.fbbgg.hs-woe.de | No |
| git.services.filesperhour.de | No |
| git.services.randers.dk | No |
| git.shaiya.net | Yes |
| git.shivering-isles.com | Yes |
| git.slashdev.space | No |
| git.sp-codes.de | No |
| git.sqdsh.top | Yes |
| git.tchncs.de | No |
| git.teknik.io | Yes |
| git.the-mcloud.ml | Yes |
| git.tilize.me | No |
| git.timshome.page | No |
| git.tldp.org | No |
| git.trackcars.org | No |
| git.user7er0.duckdns.org | Yes |
| git.usercode.de | No |
| git.vpn.ddkfm.de | Yes |
| git.vvn.space | No |
| git.wappler.systems | No |
| git.web.net | No |
| git.webhosting.rug.nl | No |
| git.webpro.ltd | No |
| git.websecure.pt | Yes |
| git.whitestores-services.co.uk | No |
| git.work-tracker.net | No |
| git.x-service.be | No |
| git.xintesa.com | Yes |
| git.xtechnology.org | No |
| git.zero-knowledge.org | No |
| git.zmctrack.net | No |
| gitdab.com | Yes |
| gitea-edumedia.evolix.org | No |
| gitea-s2i2s.isti.cnr.it | No |
| gitea.basealt.ru | No |
| gitea.cauchyma.uber.space | No |
| gitea.citizen4.eu | No |
| gitea.cloudchainsecurity.com | No |
| gitea.connexion.software | No |
| gitea.danghr.com | No |
| gitea.datahoarding.agency | No |
| gitea.ddkand.com | No |
| gitea.deliverik.com | No |
| gitea.dservice.software.ennit.de | No |
| gitea.dynvpn.de | No |
| gitea.educate.center | No |
| gitea.evolix.org | No |
| gitea.fablabchemnitz.de | No |
| gitea.gernot-payer.de | No |
| gitea.iitdh.ac.in | No |
| gitea.it | Yes |
| gitea.kaaaxcreators.de | Yes |
| gitea.knockturnmc.com | No |
| gitea.ladnet.net | No |
| gitea.marcopacs.com | No |
| gitea.math.uni-leipzig.de | No |
| gitea.moonside.games | No |
| gitea.narbonne-accessoires.fr | No |
| gitea.onl.yum-pay.com | No |
| gitea.pep.foundation | No |
| gitea.planet-casio.com | No |
| gitea.planthree.net | No |
| gitea.service.jsoude.net | No |
| gitea.service.niedersachsen.dev | No |
| gitea.services.decentm.com | Yes |
| gitea.shuishan.net.cn | No |
| gitea.stubbe.rocks | No |
| gitea.thebrokenrail.com | No |
| gitea.tobias-huebner.org | No |
| gitea.unknown.name | No |
| gitea.xinje.cc | No |
| gitee.com | No |
| github.com | No |
| github.dbalegends.com | Yes |
| github.tjshosting.com | Yes |
| gitlab.com | Yes |
| gitlab.freedesktop.org | No |
| gitlab.gnome.org | No |
| gitlab.gwdg.de | No |
| gitlab.linphone.org | No |
| gitlab.matrix.org | Yes |
| gitlab.meridian.cs.dal.ca | No |
| gitlab.nic.cz | No |
| gitlab.pagedmedia.org | No |
| gitlab.tails.boum.org | No |
| gitlab.torproject.org | No |
| gitlia.univ-avignon.fr | No |
| gittea.dev | Yes |
| inpro.informatik.uni-freiburg.de | No |
| intra.git.insite.gov.on.ca | No |
| kolaente.dev | Yes |
| kylie.cs.tu-dortmund.de | No |
| launchpad.net | No |
| libregit.org | Yes |
| linvyang.com | No |
| msg.hoppinglife.com | No |
| opendev.org | No |
| ovh-1.lucasst.work | No |
| printservice.ntgmbh.de | No |
| rbcommunity.info | Yes |
| repo.palemoon.org | Yes |
| robur.site | Yes |
| schlomp.space | No |
| scm.octopost.eu | Yes |
| source.puri.sm | No |
| source.small-tech.org | No |
| sourceforge.net | No |
| sr.ht | No |
| support.dev.procad.pl | No |
| terk.uber.space | No |
| thematrix.bw.edu | No |
| tildegit.org | No |
| tracker.adverpost.com | No |
| tracker.sagosec.com | No |
| try.gitea.io | Yes |
| unite.openworlds.info | No |
| update.ycms.pw | No |
| vmi528339.contaboserver.net | Yes |
| www.bitfork.net | No |
| www.cs.ucf.edu | Yes |
| www.git.govindgnana.com | No |
| www.github.dbalegends.com | Yes |
| www.hendrik-fichtenberger.de | No |

</details>


-----

| Type | Count |
| --- | --- | 
| Cloudflare | 50 |
| Normal | 208 |


### 19.4% of public Git services are using Cloudflare.
