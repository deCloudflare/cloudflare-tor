 ## How many % of banks & financial institutions in America are using Cloudflare?

The following is a list of banks & financial institutions in America.


<details>
<summary>_click me_

## Argentina
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Banco Credicoop | https://www.bancocredicoop.coop/ | No |
| Banco de la Nación Argentina | https://www.bna.com.ar/ | No |
| Banco Hipotecario | https://www.hipotecario.com.ar/ | No |
| Banco Itaú Argentina | https://www.itau.com.ar/ | No |
| Banco Macro | https://macro.com.ar/ | No |
| Banco Patagonia | https://www.bancopatagonia.com.ar/ | No |
| Banco Santander Río | https://www.santander.com.ar/ | Yes |
| Bank of the Province of Buenos Aires | https://www.bancoprovincia.com.ar/ | No |
| BBVA Argentina | https://www.bbva.com.ar/ | No |
| Central Bank of Argentina | https://www.bcra.gov.ar/ | No |
| Citibank Argentina | https://www.argentina.citibank.com/ | No |
| Bank of the City of Buenos Aires | https://www.bancociudad.com.ar/ | No |
| Grupo Financiero Galicia | https://www.gfgsa.com/ | No |
| HSBC Bank Argentina | https://www.hsbc.com.ar/ | No |
| Municipal Bank of Rosario | https://www.bmros.com.ar/ | No |
| New Bank of Santa Fe | https://www.bancobsf.com.ar/ | No |

</details>


<details>
<summary>_click me_

## Bahamas
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Scotiabank | https://www.scotiabank.com/ | No |
| Citibank | https://citi.com/ | No |
| CIBC FirstCaribbean International Bank | https://www.cibc.com/ | Yes |
| Royal Bank of Canada | https://rbc.com/ | No |
| Central Bank of The Bahamas | https://www.centralbankbahamas.com/ | Yes |
| Commonwealth Bank | https://commbank.com.au/ | No |
| Bank of The Bahamas International | https://bankbahamas.com/ | No |

</details>


<details>
<summary>_click me_

## Barbados
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Caribbean Development Bank | https://www.caribank.org/ | No |
| Central Bank of Barbados | https://www.centralbank.org.bb/ | No |
| CIBC FirstCaribbean International Bank | https://www.cibc.com/ | Yes |
| First Citizens Bank | https://www.firstcitizenstt.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |

</details>


<details>
<summary>_click me_

## Belize
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Belize | https://www.centralbank.org.bz/ | No |
| Belize Bank | https://www.belizebank.com/ | Yes |
| Scotiabank | https://www.scotiabank.com/ | No |
| CIBC First Caribbean International Bank | https://onlinebanking.firstcaribbeanbank.com/ | No |

</details>


<details>
<summary>_click me_

## Bermuda
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Butterfield Bank | https://www.butterfieldgroup.com/ | No |
| HSBC Bank Bermuda | https://www.hsbc.bm/ | No |
| Bermuda Commercial Bank | https://www.bcb.bm/ | No |
| Clarien Bank | https://clarienbank.com/ | No |

</details>


<details>
<summary>_click me_

## Bolivia
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Citibank | https://citi.com/ | No |
| Central Bank of Bolivia | https://www.bcb.gob.bo/ | No |
| Banco BISA | https://www.bisa.com/ | No |
| Banco de Crédito de Bolivia | https://www.bcp.com.bo/ | No |
| Banco Económico | https://www.baneco.com.bo/ | No |
| Banco Mercantil Santa Cruz | https://www.bmsc.com.bo/ | No |
| Banco Nacional de Bolivia | https://www.bnb.com.bo/ | No |
| ABN AMRO | https://www.abnamro.com/ | No |
| Banco de la Nación Argentina | https://www.bna.com.ar/ | No |
| ProCredit Bank | https://www.procreditbank.de/ | No |

</details>


<details>
<summary>_click me_

## Brazil
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Brazil | https://www.bcb.gov.br/ | No |
| Itaú Unibanco | https://www.itau.com/ | No |
| Banco Bradesco | https://banco.bradesco/ | No |
| Banco Safra | https://www.safra.com.br/ | No |
| Banco do Brasil | https://bb.com.br/ | No |
| Banrisul | https://www.banrisul.com.br/ | No |
| Caixa Econômica Federal | https://www.caixa.gov.br/ | No |
| Banestes | https://www.banestes.com.br/ | No |
| Banco Banespa | https://www.banespa.com.br/ | No |
| Banco Real | https://www.bancoreal.com.br/ | No |
| Citibank | https://citi.com/ | No |
| Santander Brasil | https://www.santander.com.br/ | No |

</details>


<details>
<summary>_click me_

## Canada
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Alterna Bank | https://alternabank.ca/ | Yes |
| Equitable Bank | https://equitablebank.ca/ | No |
| Exchange Bank of Canada | https://www.ebcfx.com/ | No |
| First Nations Bank of Canada | https://www.firstnationsbank.com/ | No |
| Home Capital Group | https://www.homecapital.com/ | No |
| HomeEquity Bank | https://www.homeequitybank.ca/ | No |
| Laurentian Bank of Canada | https://www.laurentianbank.ca/ | No |
| Manulife Bank of Canada | https://www.manulifebank.ca/ | No |
| Meridian Credit Union | https://www.meridiancu.ca/ | No |
| National Bank of Canada | https://www.nbc.ca/ | No |
| Peoples Group | https://www.peoplestrust.com/ | Yes |
| President's Choice Financial | https://www.pcfinancial.ca/ | No |
| Royal Bank of Canada | https://rbc.com/ | No |
| Tangerine Bank | https://tangerine.ca/ | No |
| Toronto-Dominion Bank | https://www.td.com/ | Yes |
| Vancity | https://vancity.com/ | No |
| VersaBank | https://www.versabank.com/ | Yes |
| Wealth One Bank of Canada | https://wealthonebankofcanada.com/ | No |
| American Express | https://www.americanexpress.com/ | No |
| Bank of China | https://www.bankofchina.com/ | No |
| MUFG Bank | https://www.bk.mufg.jp/ | No |
| Bank One Corporation | https://www.bankone.com/ | No |
| BNP Paribas | https://group.bnpparibas/ | No |
| Citibank Canada | https://www.citigroup.com/ | No |
| CTBC Bank | https://www.chinatrust.com.tw/ | No |
| Habib Bank AG Zurich | https://www.habibbank.com/ | No |
| HSBC Bank Canada | https://www.hsbc.ca/ | No |
| ICICI Bank Canada | https://www.icicibank.ca/ | No |
| Industrial and Commercial Bank of China | https://icbc.com.cn/ | No |
| JPMorgan Chase | https://www.jpmorganchase.com/ | Yes |
| Hana Financial Group | https://www.hanafn.com/ | No |
| Société Générale | https://societegenerale.com/ | Yes |
| State Bank of India | https://bank.sbi/ | No |
| Sumitomo Mitsui Banking Corporation | https://www.smbcgroup.com/ | No |
| UBS | https://www.ubs.com/ | No |
| Bank of America | https://www.bankofamerica.com/ | Yes |
| BNY Mellon | https://www.bnymellon.com/ | Yes |
| Barclays | https://www.barclays.com/ | No |
| Capital One | https://www.capitalone.com/ | No |
| China Construction Bank | https://www.ccb.com/ | No |
| Citibank | https://citi.com/ | No |
| Comerica | https://comerica.com/ | Yes |
| Deutsche Bank | https://db.com/ | Yes |
| Fifth Third Bank | https://www.53.com/ | Yes |
| M&T Bank | https://www.mtb.com/ | Yes |
| Mega International Commercial Bank | https://www.megabank.com.tw/ | No |
| Mizuho Corporate Bank | https://www.mizuhocbk.com/ | No |
| Northern Trust | https://www.northerntrust.com/ | Yes |
| PNC Financial Services | https://pnc.com/ | Yes |
| Rabobank | https://www.rabobank.com/ | No |
| State Street Corporation | https://www.statestreet.com/ | Yes |
| U.S. Bancorp | https://www.usbank.com/ | Yes |
| United Overseas Bank | https://www.uobgroup.com/ | No |
| Wells Fargo | https://www.wellsfargo.com/ | No |
| Crédit Agricole Corporate and Investment Bank | https://www.ca-cib.com/ | No |
| Credit Suisse | https://www.credit-suisse.com/ | No |
| Natixis | https://www.natixis.com/ | No |
| Silicon Valley Bank | https://svb.com/ | Yes |
| Bank of Canada | https://www.bank-banque-canada.ca/ | No |
| Business Development Bank of Canada | https://www.bdc.ca/ | Yes |
| Farm Credit Canada | https://www.fcc-fac.ca/ | No |
| ATB Financial | https://www.atb.com/ | Yes |
| Coast Capital Savings | https://www.coastcapitalsavings.com/ | No |
| Servus Credit Union | https://www.servus.ca/ | No |
| First West Credit Union | https://www.firstwestcu.ca/ | No |
| Conexus Credit Union | https://www.conexus.ca/ | No |
| Steinbach Credit Union | https://www.scu.mb.ca/ | No |
| Alterna Savings | https://alterna.ca/ | Yes |
| Affinity Credit Union | https://www.affinitycu.ca/ | No |
| Assiniboine Credit Union | https://www.assiniboine.mb.ca/ | No |
| BlueShore Financial | https://www.blueshorefinancial.com/ | No |
| UNI Financial Cooperation | https://www.uni.ca/ | No |
| DUCA Credit Union | https://duca.com/ | No |
| Libro Credit Union | https://www.libro.ca/ | No |
| Innovation Credit Union | https://www.innovationcu.ca/ | No |
| Access Credit Union | https://www.accesscu.ca/ | No |
| G&F Financial Group | https://www.gffg.com/ | No |
| Pace Savings & Credit Union | https://www.pacecu.ca/ | No |
| Canadian Imperial Bank of Commerce | https://cibc.com/ | Yes |
| Canadian Tire Financial Services | https://www.ctfs.com/ | No |
| Canadian Western Bank | https://www.cwb.com/ | Yes |
| Concentra Bank | https://concentrabank.ca/ | No |
| B2B Bank | https://b2bbank.com/ | Yes |
| Bank of Montreal | https://bmo.com/ | Yes |
| Scotiabank | https://www.scotiabank.com/ | No |
| Bridgewater Bank | https://www.bridgewaterbankmn.com/ | Yes |

</details>


<details>
<summary>_click me_

## Chile
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Chile | https://www.bcentral.cl/ | No |
| Banco del Estado de Chile | https://www.bancoestado.cl/ | Yes |
| Banco BICE | https://www.bice.cl/ | Yes |
| Banco de Chile | https://bancochile.cl/ | No |
| Banco de Crédito e Inversiones | https://www.bci.cl/ | Yes |
| Banco Falabella | https://www.bancofalabella.cl/ | Yes |
| Banco Paris | https://www.bancoparis.cl/ | No |
| Banco Ripley | https://bancoripley.com/ | No |
| Banco Santander | https://www.santander.com/ | No |
| Deutsche Bank | https://db.com/ | Yes |
| The Hongkong and Shanghai Banking Corporation | https://www.hsbc.com.hk/ | No |
| Banco de la Nación Argentina | https://www.bna.com.ar/ | No |
| Banco do Brasil | https://bb.com.br/ | No |
| Chase Bank | https://www.chase.com/ | Yes |
| MUFG Bank | https://www.bk.mufg.jp/ | No |
| Banco Bilbao Vizcaya Argentaria | https://www.bbva.com/ | No |
| Itaú Corpbanca | https://www.itau.cl/ | No |
| Banco Penta | https://www.bancopenta.cl/ | No |
| Rabobank | https://www.rabobank.com/ | No |

</details>


<details>
<summary>_click me_

## Colombia
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Bank of the Republic | https://www.banrep.gov.co/ | No |
| Bancolombia | https://www.grupobancolombia.com/ | No |
| Banco de Bogotá | https://www.bancodebogota.com/ | No |
| Davivienda | https://www.davivienda.com/ | No |
| Citibank | https://citi.com/ | No |
| Banco Bilbao Vizcaya Argentaria | https://www.bbva.com/ | No |
| Itaú Unibanco | https://www.itau.com/ | No |
| Banco AV Villas | https://www.avvillas.com.co/ | No |
| Banco de Occidente Credencial | https://www.bancooccidente.com.co/ | No |
| Banco Pichincha | https://www.pichincha.com/ | No |

</details>


<details>
<summary>_click me_

## Costa Rica
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Costa Rica | https://www.bccr.fi.cr/ | No |
| Banco de Costa Rica | https://www.bancobcr.com/ | No |
| Banco Nacional de Costa Rica | https://www.bncr.fi.cr/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Davivienda | https://www.davivienda.com/ | No |

</details>


<details>
<summary>_click me_

## Cuba
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Cuba | https://www.bc.gob.cu/ | No |
| Banco Bilbao Vizcaya Argentaria | https://www.bbva.com/ | No |
| Banco Sabadell | https://www.grupbancsabadell.com/ | No |
| Bankia | https://www.bankia.com/ | No |
| Groupe BPCE | https://www.bpce.fr/ | No |
| National Bank of Canada | https://www.nbc.ca/ | No |
| Republic Bank | https://www.republictt.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |

</details>


<details>
<summary>_click me_

## Dominica
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| National Bank of Dominica | https://www.nbdominica.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Royal Bank of Canada | https://rbc.com/ | No |
| CIBC FirstCaribbean International Bank | https://www.cibc.com/ | Yes |

</details>


<details>
<summary>_click me_

## Dominican Republic
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of the Dominican Republic | https://www.bancentral.gov.do/ | No |
| Banesco | https://banesco.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Banco Sabadell | https://www.grupbancsabadell.com/ | No |
| Banco Federal | https://www.bancofederal.com/ | No |

</details>


<details>
<summary>_click me_

## Ecuador
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Ecuador | https://www.bce.fin.ec/ | No |
| Marcel Jacobo Laniado de Wind | https://www.bancodelpacifico.com/ | No |
| Banco Pichincha | https://www.pichincha.com/ | No |
| Produbanco | https://www.produbanco.com/ | No |
| Citibank Ecuador | https://www.citibank.com.ec/ | No |

</details>


<details>
<summary>_click me_

## El Salvador
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Reserve Bank of El Salvador | https://www.bcr.gob.sv/ | No |
| Bancolombia | https://www.grupobancolombia.com/ | No |
| Banco Davivienda El Salvador | https://www.davivienda.com.sv/ | No |

</details>


<details>
<summary>_click me_

## Grenada
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Scotiabank | https://www.scotiabank.com/ | No |
| CIBC FirstCaribbean International Bank | https://www.cibc.com/ | Yes |

</details>


<details>
<summary>_click me_

## Guyana
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Bank of Guyana | https://www.bankofguyana.org.gy/ | No |
| Republic Bank | https://www.republicguyana.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Bank of Baroda | https://www.bankofbaroda.com/ | No |

</details>


<details>
<summary>_click me_

## Honduras
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Honduras | https://www.bch.hn/ | No |
| Banco de Occidente | https://www.bancocci.hn/ | No |
| Popular, Inc. | https://www.popular.com/ | No |
| Banco Azteca | https://www.bancoazteca.com/ | No |
| Lloyds Bank | https://www.lloydsbank.com/ | No |
| HSBC | https://www.hsbc.com/ | Yes |

</details>


<details>
<summary>_click me_

## Jamaica
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Bank of Jamaica | https://www.boj.org.jm/ | No |
| First Caribbean International Bank | https://onlinebanking.firstcaribbeanbank.com/ | No |
| Citibank Jamaica | https://www.citibank.com/ | No |
| The Bank of Nova Scotia | https://www.scotiabank.com/ | No |

</details>


<details>
<summary>_click me_

## Mexico
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Bank of Mexico | https://www.banxico.org.mx/ | No |
| Banca Mifel | https://www.mifel.com.mx/ | No |
| Afirme | https://www.afirme.com/ | No |
| Banco Multiva | https://www.multiva.com.mx/ | No |
| Banco Azteca | https://www.bancoazteca.com/ | No |
| BanBajío | https://www.bb.com.mx/ | No |
| Inbursa | https://www.inbursa.com.mx/ | No |
| Banorte | https://www.banorte.com/ | No |
| BanRegio | https://www.banregio.com/ | No |
| Compartamos Banco | https://www.compartamos.com/ | No |
| American Express | https://www.americanexpress.com/ | No |
| Bank of America | https://www.bankofamerica.com/ | Yes |
| Bank of China | https://www.boc.cn/ | No |
| BBVA México | https://www.bbva.mx/ | No |
| Grupo Financiero Banamex | https://www.banamex.com/ | No |
| Banco Sabadell | https://www.grupbancsabadell.com/ | No |
| Barclays | https://www.barclays.com/ | No |
| Credit Suisse | https://www.credit-suisse.com/ | No |
| Deutsche Bank | https://db.com/ | Yes |
| HSBC México | https://www.hsbc.com.mx/ | No |
| Industrial and Commercial Bank of China | https://icbc.com.cn/ | No |
| Mizuho Financial Group | https://mizuho-fg.com/ | No |
| Mitsubishi UFJ Financial Group | https://www.mufg.jp/ | No |
| Santander México | https://www.santander.com.mx/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Shinhan Bank | https://www.shinhan.com/ | No |
| UBS | https://www.ubs.com/ | No |
| Bancomext | https://www.bancomext.com/ | No |
| Banobras | https://www.banobras.gob.mx/ | No |
| Financiera Rural | https://www.gob.mx/ | No |
| Nacional Financiera | https://www.nafin.com.mx/ | No |

</details>


<details>
<summary>_click me_

## Panama
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| National Bank of Panama | https://www.banconal.com.pa/ | Yes |
| Banco Bilbao Vizcaya Argentaria | https://www.bbva.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |

</details>


<details>
<summary>_click me_

## Paraguay
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Banco Nacional de Fomento | https://www.bnf.gov.py/ | No |
| Banco BASA | https://www.bancobasa.com.py/ | No |
| Banco Atlas | https://www.bancoatlas.com.py/ | No |
| Banco Regional | https://www.regional.com.py/ | No |
| Bancop S.A. | https://www.bancop.com.py/ | No |
| Sudameris Bank | https://www.sudameris.com.py/ | No |
| Central Bank of Paraguay | https://www.bcp.gov.py/ | No |
| Citibank | https://citi.com/ | No |
| Banco do Brasil | https://bb.com.br/ | No |
| Banco de la Nación Argentina | https://www.bna.com.ar/ | No |
| Banco Bilbao Vizcaya Argentaria | https://www.bbva.com/ | No |
| Banco Itaú | https://www.itau.com.br/ | No |

</details>


<details>
<summary>_click me_

## Peru
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Reserve Bank of Peru | https://www.bcrp.gob.pe/ | No |
| Banco Azteca | https://www.bancoazteca.com/ | No |
| Banco de Crédito del Perú | https://www.viabcp.com/ | Yes |
| BBVA Perú | https://www.bbvacontinental.pe/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Interbank | https://www.interbank.pe/ | No |
| HSBC | https://www.hsbc.com/ | Yes |
| Citibank | https://citi.com/ | No |
| Banco Santander | https://www.santander.com/ | No |
| Standard Chartered | https://www.sc.com/ | No |
| Deutsche Bank | https://db.com/ | Yes |
| Banco Falabella | https://www.bancofalabella.cl/ | Yes |
| Banco Ripley | https://bancoripley.com/ | No |

</details>


<details>
<summary>_click me_

## Saint Vincent and the Grenadines
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Scotiabank | https://www.scotiabank.com/ | No |
| CIBC FirstCaribbean International Bank | https://www.cibc.com/ | Yes |
| Royal Bank of Trinidad and Tobago | https://www.rbc.com/ | No |
| United Bank Limited | https://www.ubldigital.com/ | No |

</details>


<details>
<summary>_click me_

## Saint Lucia
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| 1st National Bank of St Lucia | https://www.1stnationalbankonline.com/ | No |
| Bank of Saint Lucia | https://www.bankofsaintlucia.com/ | No |
| Scotiabank | https://www.scotiabank.com/ | No |
| Royal Bank of Canada | https://rbc.com/ | No |
| CIBC FirstCaribbean International Bank | https://www.cibc.com/ | Yes |

</details>


<details>
<summary>_click me_

## Suriname
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Suriname | https://www.cbvs.sr/ | Yes |
| De Surinaamsche Bank | https://www.dsb.sr/ | No |

</details>


<details>
<summary>_click me_

## Trinidad and Tobago
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| First Citizens Bank | https://www.firstcitizenstt.com/ | No |
| Royal Bank of Trinidad and Tobago | https://www.rbc.com/ | No |
| Republic Bank | https://www.republictt.com/ | No |

</details>


<details>
<summary>_click me_

## United states
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Ally Financial | https://www.ally.com/ | Yes |
| American Express | https://www.americanexpress.com/ | No |
| Ameriprise Financial | https://www.ameriprise.com/ | No |
| Arvest Bank | https://www.arvest.com/ | No |
| Associated Banc-Corp | https://www.associatedbank.com/ | Yes |
| Atlantic Union Bank | https://www.atlanticunionbank.com/ | No |
| BMO Harris Bank | https://www.bmoharris.com/ | No |
| BNP Paribas | https://group.bnpparibas/ | No |
| BNY Mellon | https://www.bnymellon.com/ | Yes |
| BOK Financial Corporation | https://www.bokf.com/ | Yes |
| Bank OZK | https://ozk.com/ | Yes |
| Bank of America | https://www.bankofamerica.com/ | Yes |
| Bank of Hawaii | https://boh.com/ | Yes |
| BankUnited | https://www.bankunited.com/ | No |
| Barclays | https://www.barclays.com/ | No |
| Brattleboro Savings & Loan | https://www.brattbank.com/ | No |
| Beneficial State Bank | https://www.beneficialstatebank.com/ | Yes |
| CIBC Bank USA | https://us.cibc.com/ | Yes |
| CIT Group | https://cit.com/ | No |
| Decorah Bank | https://www.decorahbank.com/ | Yes |
| Cadence Bank | https://cadencebank.com/ | No |
| Amalgamated Bank | https://www.amalgamatedbank.com/ | No |
| Clearwater Credit Union | https://www.clearwatercreditunion.org/ | Yes |
| Capital One | https://www.capitalone.com/ | No |
| Cathay Bank | https://cathaybank.com/ | No |
| Charles Schwab Corporation | https://www.aboutschwab.com/ | Yes |
| City First Bank | https://www.cityfirstbank.com/ | No |
| Citigroup | https://citigroup.com/ | No |
| Citizens Financial Group | https://www.citizensbank.com/ | Yes |
| Comerica | https://comerica.com/ | Yes |
| Commerce Bancshares | https://www.commercebank.com/ | No |
| Credit Suisse | https://www.credit-suisse.com/ | No |
| Deutsche Bank | https://db.com/ | Yes |
| Discover Financial | https://discoverfinancial.com/ | No |
| East West Bank | https://www.eastwestbank.com/ | Yes |
| F.N.B. Corporation | https://www.fnb-online.com/ | No |
| Fifth Third Bank | https://www.53.com/ | Yes |
| First Citizens BancShares | https://www.firstcitizens.com/ | Yes |
| First Hawaiian Bank | https://fhb.com/ | Yes |
| First Horizon Corporation | https://firsthorizon.com/ | No |
| First Midwest Bancorp | https://www.firstmidwest.com/ | Yes |
| First National of Nebraska | https://www.fnni.com/ | No |
| First Republic Bank | https://firstrepublic.com/ | No |
| FirstBank Holding Co | https://www.efirstbank.com/ | No |
| Flagstar Bank | https://www.flagstar.com/ | No |
| Fulton Financial Corporation | https://www.fultonbank.com/ | Yes |
| Goldman Sachs | https://goldmansachs.com/ | No |
| HSBC Bank USA | https://www.us.hsbc.com/ | Yes |
| Hancock Whitney | https://www.hancockwhitney.com/ | Yes |
| Huntington Bancshares | https://huntington.com/ | No |
| Investors Bank | https://www.myinvestorsbank.com/ | No |
| JPMorgan Chase | https://www.jpmorganchase.com/ | Yes |
| KeyBank | https://www.key.com/ | Yes |
| M&T Bank | https://www.mtb.com/ | Yes |
| MUFG Union Bank | https://www.unionbank.com/ | Yes |
| Macy's | https://macys.com/ | Yes |
| MidFirst Bank | https://midfirst.com/ | No |
| Mizuho Financial Group | https://mizuho-fg.com/ | No |
| Morgan Stanley | https://morganstanley.com/ | Yes |
| New York Community Bank | https://www.mynycb.com/ | No |
| Northern Trust | https://www.northerntrust.com/ | Yes |
| Old National Bank | https://www.oldnational.com/ | No |
| PNC Financial Services | https://pnc.com/ | Yes |
| PacWest Bancorp | https://www.pacwestbancorp.com/ | Yes |
| People's United Financial | https://www.peoples.com/ | Yes |
| Pinnacle Financial Partners | https://pnfp.com/ | Yes |
| Popular, Inc. | https://www.popular.com/ | No |
| Prosperity Bancshares | https://www.prosperitybankusa.com/ | No |
| RBC Bank | https://www.rbcbank.com/ | No |
| Raymond James Financial | https://www.raymondjames.com/ | No |
| Regions Financial Corporation | https://www.regions.com/ | Yes |
| Santander Bank | https://www.santanderbank.com/ | Yes |
| Simmons Bank | https://simmonsbank.com/ | Yes |
| South State Bank | https://www.southstatebank.com/ | Yes |
| State Farm | https://www.statefarm.com/ | No |
| State Street Corporation | https://www.statestreet.com/ | Yes |
| Sterling Bancorp | https://www.sterlingbancorp.com/ | Yes |
| Stifel | https://stifel.com/ | No |
| Synchrony Financial | https://synchrony.com/ | No |
| Synovus | https://www.synovus.com/ | No |
| TCF Financial Corporation | https://tcfbank.com/ | Yes |
| TD Bank, N.A. | https://tdbank.com/ | No |
| Teachers Insurance and Annuity Association of America | https://www.tiaa.org/ | Yes |
| Texas Capital Bank | https://www.texascapitalbank.com/ | Yes |
| Truist Financial | https://truist.com/ | Yes |
| U.S. Bancorp | https://www.usbank.com/ | Yes |
| UBS | https://www.ubs.com/ | No |
| UMB Financial Corporation | https://www.umb.com/ | Yes |
| USAA | https://www.usaa.com/ | No |
| Umpqua Holdings Corporation | https://www.umpquabank.com/ | Yes |
| United Bank | https://ubsi-inc.com/ | Yes |
| Valley Bank | https://valley.com/ | Yes |
| Washington Federal | https://www.wafdbank.com/ | No |
| Webster Bank | https://websterbank.com/ | No |
| Wells Fargo | https://www.wellsfargo.com/ | No |
| Wintrust Financial | https://wintrust.com/ | Yes |
| Zions Bancorporation | https://zionsbancorporation.com/ | Yes |
| First Green Bank | https://www.firstgreenbank.com/ | Yes |
| Lead Bank | https://lead.bank/ | No |
| Mascoma Savings Bank | https://www.mascomabank.com/ | No |
| Missoula Federal Credit Union | https://missoulafcu.org/ | Yes |
| National Cooperative Bank | https://www.ncb.coop/ | Yes |
| Suntrust Bank | https://www.suntrust.com/ | No |
| Piscataqua Savings Bank | https://piscataqua.com/ | No |
| Southern Bancorp | https://banksouthern.com/ | Yes |
| Spring Bank | https://springbankny.com/ | No |
| Sunrise Banks | https://sunrisebanks.com/ | No |
| VCC Bank | https://www.vacommunitycapital.org/ | No |
| Verity Credit Union | https://www.veritycu.com/ | No |
| VSECU | https://www.vsecu.com/ | No |
| Aspiration | https://www.aspiration.com/ | Yes |
| New Resource Bank | https://newresourcebank.com/ | Yes |
| First Boulevard | https://bankblvd.com/ | Yes |

</details>


<details>
<summary>_click me_

## Uruguay
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Uruguay | https://www.bcu.gub.uy/ | No |
| Banco de la República Oriental del Uruguay | https://www.bancorepublica.com.uy/ | No |
| Banco Hipotecario del Uruguay | https://www.bhu.net/ | No |
| Lloyds Bank | https://www.lloydsbank.com/ | No |
| Banco de la Nación Argentina | https://www.bna.com.ar/ | No |

</details>


<details>
<summary>_click me_

## Venezuela
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Central Bank of Venezuela | https://www.bcv.org.ve/ | No |
| Banco de Venezuela | https://www.bancodevenezuela.com/ | No |
| BANDES | https://www.bandes.gob.ve/ | No |
| Banco Bicentenario | https://www.bicentenariobu.com.ve/ | Yes |
| Women's Development Bank | https://www.banmujer.gob.ve/ | No |
| Bancaribe | https://www.bancaribe.com.ve/ | No |
| Banco Nacional de Crédito | https://www.bnc.com.ve/ | No |
| Banco Venezolano de Crédito | https://www.venezolano.com/ | No |
| Banesco | https://banesco.com/ | No |
| BBVA Provincial | https://www.provincial.com/ | No |
| Fondo Común | https://www.bfc.com.ve/ | No |
| Iran-Venezuela Bi-National Bank | https://ivbb.ir/ | No |
| 100% Banco | https://www.100x100banco.com/ | No |
| Mercantil Banco | https://mercantilbanco.com/ | No |
| Banco Occidental de Descuento | https://www.bod.com.ve/ | No |
| Sofitasa | https://www.sofitasa.com/ | No |

</details>

---

### By Country

| Name | Cloudflared | Normal | CF % |
| --- | --- | --- | --- |
| Argentina | 1 | 15 | 6.25% |
| Bahamas | 2 | 5 | 28.57% |
| Barbados | 1 | 4 | 20% |
| Belize | 1 | 3 | 25% |
| Bermuda | 0 | 4 | 0% |
| Bolivia | 0 | 10 | 0% |
| Brazil | 0 | 12 | 0% |
| Canada | 25 | 61 | 29.07% |
| Chile | 6 | 13 | 31.58% |
| Colombia | 0 | 10 | 0% |
| Costa Rica | 0 | 5 | 0% |
| Cuba | 0 | 8 | 0% |
| Dominica | 1 | 3 | 25% |
| Dominican Republic | 0 | 5 | 0% |
| Ecuador | 0 | 5 | 0% |
| El Salvador | 0 | 3 | 0% |
| Grenada | 1 | 1 | 50% |
| Guyana | 0 | 4 | 0% |
| Honduras | 1 | 5 | 16.67% |
| Jamaica | 0 | 4 | 0% |
| Mexico | 2 | 29 | 6.45% |
| Panama | 1 | 2 | 33.33% |
| Paraguay | 0 | 12 | 0% |
| Peru | 4 | 9 | 30.77% |
| Saint Vincent and the Grenadines | 1 | 3 | 25% |
| Saint Lucia | 1 | 4 | 20% |
| Suriname | 1 | 1 | 50% |
| Trinidad and Tobago | 0 | 3 | 0% |
| United states | 58 | 55 | 51.33% |
| Uruguay | 0 | 5 | 0% |
| Venezuela | 1 | 15 | 6.25% |


### Total

| Type | Site Count | Owner Count |
| --- | --- | --- |
| Cloudflare | 108 | 83 |
| Normal | 318 | 247 |
| Total | 426 | 330 |


### 25.35% of banks & financial institutions sites are using Cloudflare.
### 25.15% of banks & financial institutions in America are using Cloudflare.

