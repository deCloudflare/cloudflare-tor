## How many % of online video platforms are using Cloudflare?


- [Online video platform](https://en.wikipedia.org/wiki/Online_video_platform)
```
An online video platform, provided by a video hosting service, enables users to
upload, convert, store and play back video content on the Internet, often via a structured,
large-scale system that may generate revenue.
Users generally will upload video content via the hosting service's website, mobile or
desktop application, or other interface (API). 
```

Online video platforms allow users to upload, share videos or live stream their own videos to the Internet.
These can either be for the general public to watch, or particular users on a shared network.
The most popular video hosting website is YouTube.


Here's a list of online video platforms.


<details>
<summary>_click me_

## Online video platforms
</summary>

| Name | Site | Cloudflared |
| --- | --- | --- |
| Aparat | https://www.aparat.com/ | Yes |
| BitChute | https://bitchute.com/ | Yes |
| Dailymotion | https://www.dailymotion.com/ | Yes |
| EngageMedia | https://engagemedia.org/ | No |
| Flickr | https://flickr.com/ | No |
| Brighteon | https://www.brighteon.com/ | Yes |
| Rede Globo | https://redeglobo.globo.com/ | No |
| Godtube | https://www.godtube.com/ | Yes |
| Internet Archive | https://archive.org/ | Yes |
| Metacafe | https://www.metacafe.com/ | Yes |
| MetaCDN | https://www.metacdn.com/ | No |
| Niconico | https://www.nicovideo.jp/ | No |
| PeerTube | https://joinpeertube.org/ | No |
| Tencent Video | https://v.qq.com/ | Yes |
| Rumble | https://rumble.com/ | No |
| Rutube | https://rutube.ru/ | No |
| SAPO | https://www.sapo.pt/ | No |
| Tudou | https://www.tudou.com/ | No |
| Tune Pakistan | https://tune.pk/ | No |
| Vimeo | https://vimeo.com/ | No |
| Youku | https://www.youku.com/ | No |
| YouTube | https://www.youtube.com/ | No |
| AcFun | https://www.acfun.cn/ | No |
| AfreecaTV | https://www.afreecatv.com/ | No |
| Bigo Live | https://www.bigo.tv/ | No |
| Bilibili | https://bilibili.com/ | No |
| DLive | https://dlive.tv/ | No |
| iQIYI | https://www.iqiyi.com/ | No |
| Periscope | https://www.periscope.tv/ | No |
| SchoolTube | https://www.schooltube.com/ | No |
| Trilulilu | https://www.trilulilu.ro/ | Yes |
| VBOX7 | https://www.vbox7.com/ | No |
| Twitch | https://www.twitch.tv/ | No |
| Veoh | https://veoh.com/ | No |
| BongaCams | https://bongacams.com/ | Yes |
| Chaturbate | https://chaturbate.com/ | Yes |
| LiveJasmin | https://www.livejasmin.com/ | No |
| ManyVids | https://www.manyvids.com/ | Yes |
| Pornhub | https://www.pornhub.com/ | Yes |
| RedTube | https://www.redtube.com/ | Yes |
| xHamster | https://xhamster.com/ | Yes |
| XVideos | https://info.xvideos.com/ | No |
| Xtube | https://xtube.com/ | No |
| YouPorn | https://youporn.com/ | Yes |
| Facebook | https://www.facebook.com/ | No |
| SmugMug | https://www.smugmug.com/ | No |
| Instagram | https://www.instagram.com/ | No |
| Myspace | https://myspace.com/ | Yes |
| Newgrounds | https://www.newgrounds.com/ | No |
| Photobucket | https://photobucket.com/ | Yes |
| Rediff | https://www.rediff.com/ | No |
| Sina Weibo | https://weibo.com/ | No |
| Twitter | https://twitter.com/ | No |
| VK | https://vk.com/ | No |
| Wikimedia Commons | https://commons.wikimedia.org/ | Yes |
| Brightcove | https://brightcove.com/ | No |
| Imagen | https://www.imagen.io/ | No |
| Dacast | https://www.dacast.com/ | No |
| JW Player | https://jwplayer.com/ | Yes |
| Kaltura | https://www.kaltura.com/ | Yes |
| Kewego | https://corporate.kewego.com/ | No |
| MediaCore | https://mediacore.com/ | No |
| Microsoft Stream | https://products.office.com/ | No |
| Ooyala | https://www.dalet.com/ | No |
| Qumu | https://www.qumu.com/ | Yes |
| thePlatform | https://www.theplatform.com/ | No |
| IBM Cloud Video | https://video.ibm.com/ | Yes |
| ViaStreaming | https://www.viastreaming.com/ | No |
| Viddler | https://www.viddler.com/ | No |
| Vidyard | https://www.vidyard.com/ | Yes |

</details>


-----

| Type | Count |
| --- | --- | 
| Cloudflare | 24 |
| Normal | 46 |


### 34.3% of online video platforms are using Cloudflare.
