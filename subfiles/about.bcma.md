### Block Cloudflare MITM Attack

`Take action against Cloudflare`

![](../image/goodorbad.jpg)


```

This add-on will block, notify, or redirect your request if the target website is using Cloudflare.
Submit to global surveillance or resist. The choice is yours.
 
This add-on never send any data.
Your cloudflare-domain collection is yours.

```


- _Looking for `Palemoon`_? [Block Cloudflare Requests (Palemoon)](../tool/block_cloudflare_requests_pm)
- [Code](https://git.nogafam.es/deCloudflare/deCloudflare/src/branch/master/addons/code/bcma)
- Download add-on
  - From Gitea: [FirefoxESR](https://git.nogafam.es/deCloudflare/deCloudflare/raw/branch/master/addons/releases/bcma.xpi) / [Chromium / Edge](https://git.nogafam.es/deCloudflare/deCloudflare/raw/branch/master/addons/releases/bcma.crx)
