
Crimeflare `http://crimeflare.eu.org`


-----


### Permesilo

* `/addons/*` -- [MIT](addons/LICENSE)
* `/pdf/*` -- Nekonata (Vi povas trovi ekzempleron ĉie. Dankon al aŭtoroj.)
* `/tool/block_cloudflare_mitm_fx/*` -- [MIT](tool/block_cloudflare_mitm_fx/LICENSE.md)
* `/tool/block_cloudflare_requests_pm/*` -- [MIT](tool/block_cloudflare_requests_pm/LICENSE)
* `/subfiles/the_trouble_with_codeberg.md` -- [GNU AGPL](http://www.gnu.org/licenses/agpl-3.0.txt)
* Else -- [PUBLIKA DOMINO (CC0)](https://web.archive.org/web/https://creativecommons.org/share-your-work/public-domain/cc0/) = [WTFPL](http://www.wtfpl.net/about/)


[Ĉi tiu deponejo](http://crimeflare.eu.org) estis kreita anonime, publike, por uzi la mondon [kontraŭstari](https://dw.expert/2020/06/13/the-dark-side-of-google-interview-with-ex-employee-of-the-company-zach-vorhies/) [Cloudflare](https://www.cloudflare.com/).
  
Kontribuantoj, kiuj anonime [kontribuis](HISTORY.md) (inkluzive en [CryptoParty](https://cryptoparty.at/cryptoparty_wien_53)), venis antaŭen por doni al ĉi tiu projekto sian benon.
  
CC0-permesilo permesas uzi tiujn dosierojn por iu ajn uzo, eĉ en manieroj, kiujn ni povus trovi malagrablaj aŭ kontestindaj.


-----


### License

* `/addons/*` -- [MIT](addons/LICENSE)
* `/pdf/*` -- Unknown (You can find a copy everywhere. Thanks to authors)
* `/tool/block_cloudflare_mitm_fx/*` -- [MIT](tool/block_cloudflare_mitm_fx/LICENSE.md)
* `/tool/block_cloudflare_requests_pm/*` -- [MIT](tool/block_cloudflare_requests_pm/LICENSE)
* `/subfiles/the_trouble_with_codeberg.md` -- [GNU AGPL](http://www.gnu.org/licenses/agpl-3.0.txt)
* Else -- [PUBLIC DOMAIN (CC0)](https://web.archive.org/web/https://creativecommons.org/share-your-work/public-domain/cc0/) = [WTFPL](http://www.wtfpl.net/about/)


[This repository](http://crimeflare.eu.org) was created anonymously, in public, for the use of the world to [resist](https://dw.expert/2020/06/13/the-dark-side-of-google-interview-with-ex-employee-of-the-company-zach-vorhies/) [Cloudflare](https://www.cloudflare.com/).
  
Contributors who have anonymously [contributed](HISTORY.md) (including in [CryptoParty](https://cryptoparty.at/cryptoparty_wien_53)) have since come forward to give this project their blessing.
  
CC0 license allow you to use those files for any purpose, even in ways we may find distasteful or objectionable.
