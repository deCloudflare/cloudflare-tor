## Cloudflare incidents

Here's Cloudflare incidents.

[//]: # (do not edit me; start)


- 2021-06-11: [Elevated Errors in Chicago and LA](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/0cvlzpvwg251)
- 2021-06-10: [Network Performance Issues in Chicago, Atlanta, and Dallas](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/1yzvw7471qd5)
- 2021-06-08: [Network Performance Issues in India](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/0b8p5mpnn92g)
- 2021-06-08: [Network Performance Issues in Toronto and Lisbon](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/2hnqwq90dlrk)
- 2021-06-08: [Network Connectivity Issues in Dallas-Fort Worth Area.](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/106kblg0x3rm)
- 2021-06-07: [Network Performance Issues in Paris, FR (CDG)](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/4hkhqkb59vlf)
- 2021-06-04: [Increased HTTP 522 Errors in Athens, Greece](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/j2kngxpzcvpd)
- 2021-06-02: [DNS Scan is slow when onboarding zone](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/g74fyq010tnp)
- 2021-06-02: [Network Performance Issues in EWR/Newark, New Jersey](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/zbzjv8sm3g94)
- 2021-06-01: [Possible Network Congestion in Hong Kong](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/xryvrkwdk35n)
- 2021-05-31: [Possible Network Congestion in Kathmandu, Nepal - (KTM), New Delhi, India - (DEL) and Chennai, India - (MAA).](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/73znp80cmvmx)
- 2021-05-27: [Connectivity issues on Unused Prefixes](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/zcxgtvrp2tsy)
- 2021-05-25: [Load Balancing slow steering](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/qntwrjsq3k96)
- 2021-05-25: [Increased TLS Errors when using Regional Services](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/87thmgx0q3tx)
- 2021-05-25: [Network Performance Issues in India](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/ds5yfzp84zh7)
- 2021-05-24: [Distributed Web Gateway](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/psp1l248n6bk)
- 2021-05-23: [Increased HTTP 525 Errors in Chennai, India area.](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/hlbqj7dc9dnn)
- 2021-05-19: [Increased Level of DNS Query Errors](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/hmnnzb3wsm2z)
- 2021-05-19: [Possible Network Congestion in Mumbai, India area.](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/pkcqpwdvqwg4)
- 2021-05-18: [DNS Queries Issue](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/qk2x3w1rr14v)
- 2021-05-18: [Cloudflare 1.1.1.1 Issues](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/9fs0xpvty9k9)
- 2021-05-14: [HTTP 500 errors for Image Resizing for certain JPEG images with EXIF metadata](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/lbg823p4xqj1)
- 2021-05-12: [Cloudflare Pages Intermittent Build Failures](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/ngmzl23qwzpw)
- 2021-05-11: [1.1.1.1 for Families DoH Connectivity Issues](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/wsxjrx35wqnr)
- 2021-05-10: [Elevated number of 522 errors in LHR, London](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/pwy4ncdyljwt)
- 2021-05-10: [Network Performance Issues in Atlanta, GA, United States - (ATL)](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/27b5g91ymk3s)
- 2021-05-08: [Network Performance Issues in ICN01, South Korea](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/p54991rw500p)
- 2021-05-06: [Elevated number of 522 errors in MIA Miami Florida](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/3jk139l178h8)
- 2021-05-04: [Network Performance Issues](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/ct6nps84wll3)
- 2021-04-30: [Distributed Web Gateway Issue](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/wvnbyw9vc2ft)
- 2021-04-29: [Network Performance Issues in Bogotá and Medellín](https://web.archive.org/web/https://www.cloudflarestatus.com/incidents/6xd7zv5hl5ww)
- 2021-04-28: [Distributed Web Gateway](https://www.cloudflarestatus.com/incidents/c3nxskjd4ypm)
- 2021-04-28: [Network Performance Issues in São Paulo and Rio de Janeiro](https://www.cloudflarestatus.com/incidents/wrmr2jpcfc6l)
- 2021-04-27: [Network Performance Issues in Mumbai, India area.](https://www.cloudflarestatus.com/incidents/qqpy816xd2b3)
- 2021-04-26: [Degraded Performance Issues in Mumbai.](https://www.cloudflarestatus.com/incidents/dkh7j5hb0zsn)
- 2021-04-26: [Network Performance Issues in Mumbai, India area.](https://www.cloudflarestatus.com/incidents/rqgwbms5kgx9)
- 2021-04-26: [Increased HTTP 520 Errors](https://www.cloudflarestatus.com/incidents/8sgqpncp8qvy)
- 2021-04-24: [Increased HTTP 522 Errors in Milan, Italy - (MXP)](https://www.cloudflarestatus.com/incidents/p9cr779vv2mb)
- 2021-04-21: [Distributed Web Gateway](https://www.cloudflarestatus.com/incidents/fw25q1t7mplk)
- 2021-04-20: [Increased HTTP 500 Errors in Lisbon and Toronto](https://www.cloudflarestatus.com/incidents/b8g7jbq70hzd)
- 2021-04-20: [Customer Impacting Issue with SZX](https://www.cloudflarestatus.com/incidents/mw14qc5m4yq7)
- 2021-04-19: [Retro: Elevated number of 522 errors in BOM](https://www.cloudflarestatus.com/incidents/zv7z928b3vpk)
- 2021-04-19: [Retro: Congestion in Ashburn, Virginia](https://www.cloudflarestatus.com/incidents/5gsc6k32vflf)
- 2021-04-17: [Increased Errors in Cloudflare's Mombasa Datacenter](https://www.cloudflarestatus.com/incidents/fntfm836z3d8)
- 2021-04-17: [Users in South Africa and the APAC region may experience high latency and connection timeouts](https://www.cloudflarestatus.com/incidents/sw00f0rq7r7y)
- 2021-04-16: [Cloudflare Gateway Connectivity issues](https://www.cloudflarestatus.com/incidents/dnzyrx6p3dcf)
- 2021-04-16: [Network Congestion In Several Asia Data Centers](https://www.cloudflarestatus.com/incidents/vcjnnzwzpk2x)
- 2021-04-14: [Australian users experienced elevated errors and a drop in 200s](https://www.cloudflarestatus.com/incidents/bmk3424q74z6)
- 2021-04-14: [Elevated number of 530/503 errors from Amsterdam, Netherlands - (AMS)](https://www.cloudflarestatus.com/incidents/c9zky7zrc52v)
- 2021-04-13: [Cloudflare Name Resolver](https://www.cloudflarestatus.com/incidents/nt7vx1ll7nht)
- 2021-04-11: [Connectivity issues around Manchester, United Kingdom](https://www.cloudflarestatus.com/incidents/0bzzgss08v97)
- 2021-04-11: [Network Performance Issues in Incheon (ICN), South Korea](https://www.cloudflarestatus.com/incidents/cm4yk2ztmjkg)
- 2021-04-09: [SSL handshake failures in the China region](https://www.cloudflarestatus.com/incidents/zjg9cr3pw40h)
- 2021-04-08: [Cloudlare DNS NODATA change](https://www.cloudflarestatus.com/incidents/x4v5d51wpcnr)
- 2021-04-08: [Browser isolation service degraded](https://www.cloudflarestatus.com/incidents/14rjb6jf7mwh)
- 2021-04-02: [DNS resolution issue](https://www.cloudflarestatus.com/incidents/ctfqh8z7ghqs)
- 2021-03-31: [Websocket connectivity Issues](https://www.cloudflarestatus.com/incidents/nv0gd0k8r9lf)
- 2021-03-29: [Network performance issues in Canada may result in increased latency](https://www.cloudflarestatus.com/incidents/v40553yyq80g)
- 2021-03-29: [SSL/TLS Handshake failures](https://www.cloudflarestatus.com/incidents/44p4slgcfhmz)
- 2021-03-29: [Network performance issues in South-America may result in increased latency](https://www.cloudflarestatus.com/incidents/h2gr2y2q7m96)
- 2021-03-28: [Network Performance Issues in Toronto (ON) and Montréal (QC), Canada](https://www.cloudflarestatus.com/incidents/qjwxv73chh5b)
- 2021-03-24: [Network Performance Issues in Ashburn, Virginia, USA](https://www.cloudflarestatus.com/incidents/zxgr2bz84qdb)
- 2021-03-24: [Increased HTTP Errors in multiple locations](https://www.cloudflarestatus.com/incidents/bzn7ybpsvkr8)
- 2021-03-24: [Connectivity Issues observed in Lisbon](https://www.cloudflarestatus.com/incidents/qkjzg7nyhckz)
- 2021-03-23: [Increased HTTP 522 Errors in Ashburn](https://www.cloudflarestatus.com/incidents/s298qs7mtqmp)
- 2021-03-23: [Network Performance Issues in Ashburn, VA, United States.](https://www.cloudflarestatus.com/incidents/v631vf20c6f9)
- 2021-03-22: [Increased HTTP 522 Errors in Stockholm](https://www.cloudflarestatus.com/incidents/35z4z0003y5f)
- 2021-03-18: [Network Performance Issues in Buenos Aires, Argentina](https://www.cloudflarestatus.com/incidents/qm3tv6cpp83t)
- 2021-03-18: [Increased HTTP 520 Errors](https://www.cloudflarestatus.com/incidents/9k01th8fs6yv)
- 2021-03-17: [Customer Impacting Issue in European Region](https://www.cloudflarestatus.com/incidents/0fvbh23lqlz2)
- 2021-03-17: [Network Latency Issues in the Pacific North West region.](https://www.cloudflarestatus.com/incidents/hf8h9cg8lb4p)



[//]: # (do not edit me; end)


-----

## Other


*2021.04.04*

- **Censorship**: Codeberg.org silently deleted `cloudflare-tor` and all related forks from codeberg.org without joining the discussion.
  - [Codeberg's Attack on Transparency and on Cloudflare Opposition](subfiles/the_trouble_with_codeberg.md)
- **Please**, try not to link to git directly. Link to URL forwarder, `http://crimeflare.eu.org` instead.

*2021.03.28*

- **Censorship**: Codeberg.org ordered Jeff to delist codeberg.org related lines, yet they failed to mention which lines are the problem. All codeberg lines are removed for now.

*2020.05.19*

- Tor Project member, [Jim Newsome](https://twitter.com/sporksmith) (_[Shadow](https://shadow.github.io/) dev/jnewsome_), [deleted cloudflare warning](https://trac.torproject.org/projects/tor/wiki/org/projects/WeSupportTor?action=diff&version=463&old_version=462), putting Tor users at risk.
- And he [replaced "dead link" to cloudflare's link](https://trac.torproject.org/projects/tor/wiki/org/projects/DontBlockMe?action=diff&version=38&old_version=37).
  - _Hey Tor Project, it's not "dead". You deleted them, remember?_


*2020.05.13*

The Tor Project [deleted](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) important ticket, [number 24351](https://trac.torproject.org/projects/tor/ticket/24351) after the spammer replaced it with child porn images which hosted on the Tor Onion service. [Ticket 34175](https://trac.torproject.org/projects/tor/ticket/34175).
  - Tor project member, Gustavo Gus (_Community Team Lead/ggus_) defacted Cloudflare-related documents after few days later.

*2020.02.25*

Self publishing platform, [BookRix.com](https://www.bookrix.com/), denied Mr. Jeff Cliff's book titled "_The Great Cloudwall_" and stopped publishing it. The reason is "_Copyright infringement_". All of book's data, text and image can be found in this repository. You can download this book [here](pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub).


*2019.08.05*

@jeffcliff@niu.moe:
```
#greatcloudwall gets further politicized:
Terminates Service for 8Chan - the #greatcloudwall gets one step closer to dictating what you can or cannot say on the internet 
``` 

https://niu.moe/@jeffcliff/102566376973167221

*2019.04.04*

@holydevil@twitter:
```
The log file from your iOS app shows that you do track personal information, like device name, UUID, and an identifier - install_id That contradicts what you have in your blogpost - “We don't write user-identifiable log data to disk”
```

https://twitter.com/holydevil/status/1112769739045158912

*2019.03.13*

@thexpaw@twitter:
```
So why did I get that email anyway if I'm opted out of all email communication in the account settings?
Which part of the privacy policy allows you to share data with marketing crap like trustpilot?
```

https://twitter.com/thexpaw/status/1108424723233419264

*2019.03.12*

```Cool new tool : Cloud Firewall```

https://framapiaf.org/@gkrishnaks/101727497214557035

*2019.03.03*

```
"I can't visit Army.mill and Archive Today with Cloudflare DNS."
```

https://twitter.com/wongmjane/status/1102446734993551360

Let's test: https://danwin1210.me/url.php?id=39706

```
archive.today
CloudFlare: (None)
Google: A 46.17.42.43 (Russia, AS51659 LLC Baxet)
```

```
archive.is
CloudFlare: A 104.16.181.15 (NSA, AS13335 Cloudflare, Inc.)
Google: A 46.17.42.43 (Russia, AS51659)
```

Why these results are completely different!?
Try OpenNIC: https://www.opennic.org/


*2019.03.02*

"client was trying to make session requests to their API servers
but before I can prevent it, cloudflare had me blocked.
It's pretty bad as half the internet is behind Cloudflare."

https://twitter.com/Skyfusion89/status/1101596592426151937

*2019.02.27*

* Cloudflare XSS bypass
https://twitter.com/ameenmaali/status/1100536056372490241

*2019.02.26*

Take a look at Cloudflare's transparency report, "Some things we have never done" section.

```
Cloudflare has never terminated a customer or taken down content due to political pressure.*
```

If you're using SumatraPDF, you won't notice * is a link to https://www.cloudflare.com/cloudflare-criticism/ .
Apparently they've terminated a political account.
Do you think it's okay to make a false statement and hide a link to tiny asterisk?

https://twitter.com/mattskala/status/1100479615389159424
https://mstdn.io/@mattskala/101660051818948847

*2019.02.24*

```
"Sites that respect their visitors do not resort to Cloudflare."
"In some cases, for particular countries, having all traffic visible
to the U.S.A can be a matter of life and death."
```
http://techrights.org/2019/02/17/the-cloudflare-trap/

*2019.02.21*

* CF defaults to HTTP connections for its customers
https://g0v.social/@sheogorath/101404226960335320

*2019.02.08*

* well written post, along with some causes for action in privacytools.io
https://github.com/privacytoolsIO/privacytools.io/issues/374#issuecomment-460077544

* another privacytools.io thread
https://github.com/privacytoolsIO/privacytools.io/issues/711

* Cryptome on CF's ability to deanonymize (2016)
https://cryptome.org/2016/07/cloudflare-de-anons-tor.htm

* bug report issued in wire webapp
https://github.com/wireapp/wire-webapp/issues/5716

*2019.02.01*

* The global internet is rotting from within, and 

"In a not-so-distant future, if we're not there already, it may be that if you're
going to put content on the Internet you'll need to use a company with a giant
network like Cloudflare, Google, Microsoft, Facebook, Amazon, or Alibaba."

Net neutrality is but a skirmish in this larger struggle for control to monopolize all global digital communications.
https://www.itu.int/en/ITU-T/Workshops-and-Seminars/20181218/Documents/Geoff_Huston_Presentation.pdf

*2018.12.10*

* "like this page on NSA/Facebook & allow Facebook to track you to prove you're a human being"
https://niu.moe/@jeffcliff/101220470739320859


*2018.11.12*

* Cloudflare's permissions for DNS are...sketchy
https://weeaboo.space/objects/323a4b45-6e40-44f0-9108-77245638df7e

* AV Updates blocked by cloudflare - "how to proceed?"
http://forums.clamwin.com/viewtopic.php?t=4915
http://lists.clamav.net/pipermail/clamav-users/2018-November/thread.html

---

### Previous 


This project was started by Marie Gutbub (Shiro) ([@shiromarieke](https://twitter.com/shiromarieke)) and CryptoParty Berlin ([@cryptopartybln](https://twitter.com/CryptoPartyBER)).

Shiro handed over support to the greater internet in summer 2016.


It was a text list on okfn.org text pad: (https://pad.okfn.org/p/cloudflare-tor and https://pad.okfn.org/p/noncloudflare-torblocks)


However it was pointed out that this website itself was on Cloudflare, and cloudflare actively blocked Tor users to access it.


So it was moved to systemli.org : ( https://pad.systemli.org/p/noncloudflare-torblocks )
and an onion service ( http://j7652k4sod2azfu6.onion/p/noncloudflare-torblocks , http://j7652k4sod2azfu6.onion/p/cloudflare-tor , http://j7652k4sod2azfu6.onion/p/cloudflare-philosophy )


These systemli pads needed to be updated once in every while (week? month?) or the whole list was scrubbed.  
This happened a few times, and there were some attempts at vandalism up to and including june 2016.
 
 
A fork was made during a multi-day outage at http://git.vola7ileiax4ueow.onion/fuckcloudflare/cloudflare-tor/ (new, not GitHub repo)


However vola7ileiax4ueow's git service went down, so it was moved to github


There used to also be a list of websites that *were* on cloudflare but are no longer.
This list has been lost ( it was on https://pad.systemli.org/p/ex-cloudflare-tor )
