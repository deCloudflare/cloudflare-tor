- [Block Cloudflare MITM Attack](../subfiles/about.bcma.md) _v1.0.6_
- [Are links vulnerable to MITM attack?](../subfiles/about.ismm.md) _v1.0.23.7_
- [Will these links block Tor user?](../subfiles/about.isat.md) _v1.0.4.6_
- [Which website rejected me?](../subfiles/about.urjm.md) _v1.0.5.4_


-----

### Aldonaĵo por Firefox/Chromium/Edge

- Funkcias plej bone kun plej nova [Tor Browser](https://www.torproject.org/download/)(rekomendas) aŭ [Mozilla Firefox _ESR_](https://portableapps.com/apps/internet/firefox-portable-esr)
- [Permesilo](LICENSE)


-----

### Addons for Firefox/Chromium/Edge

- Works best with latest [Tor Browser](https://www.torproject.org/download/)(recommend) or [Mozilla Firefox _ESR_](https://portableapps.com/apps/internet/firefox-portable-esr)
- [License](LICENSE)
